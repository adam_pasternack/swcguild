
#div1 {
  height: 150px;
  width: 400px;
  margin: 20px;
  border: 1px solid red;
  padding: 10px;
}

For #div1:

Total height: element height + 2(border thickness) + 2(padding) + 2(margin) = 150 + 2 + 20 + 40 = 212px 

Total width: element width + 2(border thickness) + 2(padding) + 2(margin) = 400 + 2 + 20 + 40 = 462px

Browser calculated height: element height + 2(border thickness) + 2(padding) = 150 + 2 + 20 = 172px

Browser calculated width: element width + 2(border thickness) + 2(padding) = 400 + 2 + 20 = 422px